# desenhos-animados

Um projeto de clojure para disciplina de projetos interdisciplinar. Roda e tem as dependencias
cotroladas por leiningen. Mas o projeto conta com uma esteira de CI/CD no 
[gitlab](https://gitlab.com/samosaara/desenho_animado_clojure/-/jobs) onde pode-se baixar jars
executaveis pré-compilados.

## Licença

Projeto distribuído sobre a GPLv3.
