(ns desenhos-animados.tela-pesquisa
  (:use seesaw.core
        [seesaw.table :only [table-model, clear!, insert-at!]]
        [seesaw.bind :only [bind, transform]]
        [seesaw.mig :only [mig-panel]])
  (:import javax.swing.JTable
           javax.swing.JFrame
           java.awt.Component))

(def lista-desenhos (atom []))
(def desenhos-por-idade (atom []))
(def total-estoque (atom 0))
(def faixa (atom 0))

(defn bound-component
  "seesaw bind sour/ce to target and returns source, unless target is a Component then it
  returns target instead"
  [source target]
  (bind source target)
  (if (instance? Component target) target source))

(def desenhos-model
  (table-model :columns [
                         {:key :nome :text "Nome do desenho" :class String}
                         {:key :preco :text "Preço" :class Double}
                         {:key :ano :text "Ano" :class Long}]
               :rows []))

(defn filter-faixa-etaria "Filtra a lista de desenhos pela faixa etaria"
  [& args]
  (reset! desenhos-por-idade
          (vec (filter #(= (nth args 3) (:faixa-etaria %)) @lista-desenhos)))
  (clear! desenhos-model)
  (doseq [[i, des] (map-indexed vector @desenhos-por-idade)]
    (insert-at! desenhos-model i des)))

(add-watch faixa :nova-idade filter-faixa-etaria)

(def painel-pesquisa
  (mig-panel
    :constraints ["", "[al right][grow, fill, al center][al left][]", "[][grow]"]
    :items [
            [(label "<html><h3>Desenhos por faixa etária</h3></html>"), "span, al center, growx, wrap"]
            [(label "Faixa etária")]
            [(bound-component (spinner :model (spinner-model 0 :from 0 :to 18)) faixa), "span, wrap"]
            [(scrollable (let [tabela (table :model desenhos-model :show-grid? true :minimum-size [0 :by 100])]
                           (.setAutoCreateRowSorter tabela true)
                           tabela)
                         :minimum-size [0 :by 100]), "span, grow, wrap"]
            [(separator), "span, growx, wrap"]
            [(let [valTtl (label)]
               (bind total-estoque
                     (transform #(str "O valor total em estoque é R$ " %))
                     valTtl)
               valTtl), "span, growx, wrap"]]))

(defn- fecha-janela-pesquisa [_]
  (reset! faixa 0))

(defn mostra-pesquisa
  ([pai desenhos]
   (reset! lista-desenhos desenhos)
   (reset! faixa 0)
   (reset! total-estoque
           (reduce #(+ %1 (* (:quantidade %2) (:preco %2))) 0 desenhos))
   (let [janela (custom-dialog :title "Pesquisa"
                               :on-close :hide
                               :parent pai
                               :modal? true
                               :visible? false
                               :content painel-pesquisa)]
     (listen janela :component-hidden fecha-janela-pesquisa)
     (add! janela (label (str "A média de duração é "
                              (/ (reduce + (map :minutos desenhos))
                                 (count desenhos)))))
     (pack! janela)
     (.setMinimumSize janela (.getSize janela))
     (.setLocationRelativeTo janela nil)
     janela)))
